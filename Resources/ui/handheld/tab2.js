exports.create = function(model) {
 
    var self = Ti.UI.createView({
        backgroundColor : '#fff',
        layout : 'vertical',
        top : '0dp',
        height : Ti.UI.FILL,
        width : Ti.UI.FILL
    });
    
    var Label = Ti.UI.createLabel({
		width : Ti.UI.FILL,
		height : Ti.UI.SIZE,
		font : {
			fontFamily : 'Arial',
			fontSize : 38,
			fontWeight : 'bold'
		},
		color : 'blue',
		text : 'Tab 2',
		textAlign : 'center'
	});
	self.add(Label);
	
    return self;
};
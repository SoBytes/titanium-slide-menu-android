function ApplicationTabGroup(Window) {

	var SlideMenu = require('ti.nartex.slidemenu');

	var self = Ti.UI.createWindow({
		exitOnClose : true,
		navBarHidden : true
	});

	var leftView = Ti.UI.createScrollView({
		width : Ti.UI.FILL,
		height : Ti.UI.FILL,
		backgroundColor : '#666',
		contentHeight : 'auto',
		scrollType : 'vertical'
	});

	var rightView = Ti.UI.createScrollView({
		width : Ti.UI.FILL,
		height : Ti.UI.FILL,
		backgroundColor : 'red',
		contentHeight : 'auto',
		scrollType : 'vertical'
	});

	var leftViewContent = Ti.UI.createView({
		height : Ti.UI.SIZE,
		left : '5dp',
		right : '5dp',
		layout : 'vertical'
	});
	leftView.add(leftViewContent);

	var mainView = Ti.UI.createView({
		backgroundColor : '#fff',
		width : Ti.UI.FILL,
		height : Ti.UI.FILL
	});
	
	var navBar = Ti.UI.createView({
		backgroundColor : '#f8f9fa',
		top : '0dp',
		width : Ti.UI.FILL,
		height : '60dp'
	});

	navBar.add(Ti.UI.createView({
		backgroundColor : '#d9d9d9',
		width : '100%',
		height : 2,
		bottom : 0
	}));

	mainView.add(navBar);

	navBar.add(Ti.UI.createLabel({
		color : '#424242',
		font : {
			fontFamily : 'Arial',
			fontSize : 18,
			fontWeight : 'bold'
		},
		textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
		text : 'Tab Menu'
	}));

	var buttonOpenLeft = Ti.UI.createView({
		zIndex : 2,
		width : '50dp',
		height : '50dp',
		left : '5dp'
	});

	buttonOpenLeft.add(Ti.UI.createLabel({
		color : '#95989c',
		font : {
			fontSize : 40
		},
		text : 'o',
		textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER
	}));
	
	buttonOpenLeft.addEventListener('click', function() {
		if (slideMenuView.state == SlideMenu.STATE_DEFAULT) {
			slideMenuView.state = SlideMenu.STATE_LEFT_VISIBLE;
		} else {
			slideMenuView.state = SlideMenu.STATE_DEFAULT;
		}
	});

	navBar.add(buttonOpenLeft);
	
	var mainContentView = Ti.UI.createView({
		backgroundColor : '#fff',
		top : '60dp',
		width : Ti.UI.FILL,
		height : Ti.UI.FILL
	});
    
    mainView.add(mainContentView);
    
	mainContentView.open = function(currentView){
		if (mainContentView && mainContentView.children != undefined)
		{   
		    // Save childrens       
		    var removeData = [];
		    for (i = mainContentView.children.length; i > 0; i--){
		        removeData.push(mainContentView.children[i - 1]);  
		    };
		 
		    // Remove childrens
		    for (i = 0; i < removeData.length; i++){
		        mainContentView.remove(removeData[i]);
		    }
		    removeData = null;
		};
	    mainContentView.add(currentView);
	};
	
	var slideMenuView = SlideMenu.createSlideMenu({
		centerView : mainView,
		leftView : leftView,
		rightView : rightView,
		menuWidth : '300dp'
	});
	self.add(slideMenuView);

	slideMenuView.addEventListener("stateChanged", function(e) {
		var state;
		switch (e.state) {
			case SlideMenu.STATE_LEFT_VISIBLE:
				state = 'LEFT_VISIBLE';
				break;
			case SlideMenu.STATE_DEFAULT:
				state = 'DEFAULT';
				break;
			case SlideMenu.STATE_RIGHT_VISIBLE:
				state = 'RIGHT_VISIBLE';
				break;
		}
	});
    
    /*
     * Left menu tabs
     */
	var hideMenuButton = Ti.UI.createButton({
		top : '5dp',
		width : Ti.UI.FILL,
		height : Ti.UI.SIZE,
		title : 'Hide menu'
	});
	
	hideMenuButton.addEventListener('click', function() {
		slideMenuView.state = SlideMenu.STATE_DEFAULT;
	});

	leftViewContent.add(hideMenuButton);
	
	var tab1 = Ti.UI.createButton({
		top : '5dp',
		width : Ti.UI.FILL,
		height : Ti.UI.SIZE,
		title : 'Tab 1'
	});
	
	tab1.addEventListener('click', function() {
		slideMenuView.state = SlideMenu.STATE_DEFAULT;
		mainContentView.open(require('ui/handheld/tab1').create({}));
	});

	leftViewContent.add(tab1);
	
	var tab2 = Ti.UI.createButton({
		top : '5dp',
		width : Ti.UI.FILL,
		height : Ti.UI.SIZE,
		title : 'Tab 2'
	});
	
	tab2.addEventListener('click', function() {
		slideMenuView.state = SlideMenu.STATE_DEFAULT;
		mainContentView.open(require('ui/handheld/tab2').create({}));
	});

	leftViewContent.add(tab2);
	
	var tab3 = Ti.UI.createButton({
		top : '5dp',
		width : Ti.UI.FILL,
		height : Ti.UI.SIZE,
		title : 'Tab 3'
	});
	
	tab3.addEventListener('click', function() {
		slideMenuView.state = SlideMenu.STATE_DEFAULT;
		mainContentView.open(require('ui/handheld/tab3').create({}));
	});

	leftViewContent.add(tab3);
	
	var tab4 = Ti.UI.createButton({
		top : '5dp',
		width : Ti.UI.FILL,
		height : Ti.UI.SIZE,
		title : 'Tab 4'
	});
	
	tab4.addEventListener('click', function() {
		slideMenuView.state = SlideMenu.STATE_DEFAULT;
		mainContentView.open(require('ui/handheld/tab4').create({}));
	});

	leftViewContent.add(tab4);
	
	var tab5 = Ti.UI.createButton({
		top : '5dp',
		width : Ti.UI.FILL,
		height : Ti.UI.SIZE,
		title : 'Tab 5'
	});
	
	tab5.addEventListener('click', function() {
		slideMenuView.state = SlideMenu.STATE_DEFAULT;
		mainContentView.open(require('ui/handheld/tab5').create({}));
	});

	leftViewContent.add(tab5);

	return self;
};

module.exports = ApplicationTabGroup;
